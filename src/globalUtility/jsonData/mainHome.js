import { imageUtil } from "../imageHelper";
const images = imageUtil.images;

export const mainHome = {
  uniqueHomePage: [
    {
      image: images.FashionHome1,
      redirectUrl: "/layout1",
      title: "Home page",
      isNew: false,
      updateVersion: ""
    },
    {
      image: images.FashionHome2,
      redirectUrl: "/home2",
      title: "Home page 2",
      isNew: false,
      updateVersion: "1.1"
    },
    {
      image: images.FashionHome3,
      redirectUrl: "/home3",
      title: "Home page 3",
      isNew: false,
      updateVersion: "1.2"
    },
    {
      image: images.FashionHome4,
      redirectUrl: "/home4",
      title: "Home page 4",
      isNew: false,
      updateVersion: "1.3"
    },
    {
      image: images.FashionHome5,
      redirectUrl: "/home5",
      title: "Home page 5",
      isNew: true,
      updateVersion: "1.4"
    },
    {
      image: images.FashionHome6,
      redirectUrl: "/home6",
      title: "Home page 6",
      isNew: true,
      updateVersion: "1.5"
    },
    {
      image: images.FashionHome7,
      redirectUrl: "/home7",
      title: "Home page 7",
      isNew: true,
      updateVersion: "1.6"
    }
  ],
  fashionStoreList: [
    {
      image: images.FashionHome1,
      path: "/home1",
      title: "Home page",
      activeCategory: "home"
    },
    {
      image: images.FashionHome2,
      path: "/home2",
      title: "Home page 2",
      activeCategory: "home"
    },
    {
      image: images.FashionHome3,
      path: "/home3",
      title: "Home page 3",
      activeCategory: "home"
    },
    {
      image: images.FashionHome4,
      path: "/home4",
      title: "Home page 4",
      activeCategory: "home"
    },
    {
      image: images.FashionHome5,
      path: "/home5",
      title: "Home page 5",
      activeCategory: "home"
    },
    {
      image: images.FashionHome6,
      path: "/home6",
      title: "Home page 6",
      activeCategory: "home"
    },
    {
      image: images.FashionHome7,
      path: "/home7",
      title: "Home page 7",
      activeCategory: "home"
    },
    {
      image: images.FashionMenCategory,
      path: "/category?type=men-mix",
      title: "Men's category page",
      activeCategory: "category"
    },
    {
      image: images.FashionWomenCategory,
      path: "/category?type=men-mix",
      title: " Women's category page",
      activeCategory: "category"
    },
    {
      image: images.fashionLandBlog,
      path: "/blogs/blog",
      title: "Blog Page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogLeft,
      path: "/blogs/blog-left-side",
      title: "Blog left-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogRight,
      path: "/blogs/blog-right-side",
      title: "Blog right-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogLeftRight,
      path: "/blogs/blog-left-right-side",
      title: "Blog left-right-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogGrid,
      path: "/blogs/blog-2-grid",
      title: "Blog 2 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGrid,
      path: "/blogs/blog-2-grid-left-side",
      title: "Blog 2 grid left-side page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGridRight,
      path: "/blogs/blog-2-grid-right-side",
      title: "Blog 2 grid right-side page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGrid3,
      path: "/blogs/blog-3-grid",
      title: "Blog 3 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGrid4,
      path: "/blogs/blog-4-grid",
      title: "Blog 4 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGridSimple2,
      path: "/blogs/blog-2-grid-simple",
      title: "Blog 2 grid simple page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGridSimple3,
      path: "/blogs/blog-3-grid-simple",
      title: "Blog 3 grid simple page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogGridSimple4,
      path: "/blogs/blog-4-grid-simple",
      title: "Blog 4 grid simple page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandBlogList,
      path: "/blogs/blog-list",
      title: "Blog list page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandBlogListLeft,
      path: "/blogs/blog-left-side",
      title: "Blog list left-side page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandBlogListRight,
      path: "/blogs/blog-right-side",
      title: "Blog list right-side page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandBlogListLeftRight,
      path: "/blogs/blog-list-left-right-side",
      title: "Blog list left-right page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandBlogClassic,
      path: "/blogs/blog-classic",
      title: "Blog classic page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogLeftSticky,
      path: "/blogs/blog-left-side-sticky",
      title: "Blog sticky left-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogRightSticky,
      path: "/blogs/blog-right-side-sticky",
      title: "Blog sticky right-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandBlogLeftRightSticky,
      path: "/blogs/blog-left-right-side-sticky",
      title: "Blog sticky left-right-side page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandPro2grid,
      path: "/products/product-2-grid",
      title: "Product 2 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandPro2GridLeft,
      path: "/products/product-2-grid-left-side",
      title: "Product 2 grid left-side page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandPro2GridLeftRight,
      path: "/products/product-2-grid-right-side",
      title: "Product 2 grid right-side page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandPro3Grid,
      path: "/products/product-3-grid",
      title: "Product 3 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandPro4Grid,
      path: "/products/product-4-grid",
      title: "Product 4 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandPro6Grid,
      path: "/products/product-6-grid-full",
      title: "Product 6 grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandAllInOne,
      path: "/products/all-in-one-grid",
      title: "Product all grid page",
      activeCategory: "grid2"
    },
    {
      image: images.fashionLandProList,
      path: "/products/product-list",
      title: "Product list page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandProListLeft,
      path: "/products/product-list-left-side",
      title: "Product list left-side page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandProListRight,
      path: "/products/product-list-right-side",
      title: "Product list right-side page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandProListAdv,
      path: "/products/product-list-advance",
      title: "Product list-advance page",
      activeCategory: "list"
    },
    {
      image: images.fashionLandProDetail,
      path: "/products/product-detail",
      title: "Product detail page",
      activeCategory: "detail"
    },
    {
      image: images.fashionLandProDetailSimple,
      path: "/products/product-detail-simple",
      title: "Product detail simple page",
      activeCategory: "detail"
    },
    {
      image: images.fashionLandProDetailSlider,
      path: "/products/product-detail-slider",
      title: "Product detail slider page",
      activeCategory: "detail"
    },
    {
      image: images.fashionLandProDetailSticky,
      path: "/products/product-detail-sticky",
      title: "Product detail sticky page",
      activeCategory: "detail"
    },
    {
      image: images.fashionLandProDetailZoom,
      path: "/products/product-detail-with-zoom-effect",
      title: "Product detail with zoom effect",
      activeCategory: "detail"
    },
    {
      image: images.fashionLandProComparision,
      path: "/products/product-comparision",
      title: "Product comparision page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandProComparisionAdv,
      path: "/products/product-comparision-adv",
      title: "Product comparision page 2",
      activeCategory: "other"
    },
    {
      image: images.fashionLandProSearch,
      path: "/category?type=men-footwear",
      title: "Product search page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandProSearch2,
      path: "/category?type=men-footwear",
      title: "Product search page 2",
      activeCategory: "other"
    },
    {
      image: images.fashionLandAboutUs,
      path: "/about-us",
      title: "About us page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandContactUs,
      path: "/contact-us",
      title: "Contact us page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandContactUs2,
      path: "/contact-us",
      title: "Contact us page 2",
      activeCategory: "other"
    },
    {
      image: images.fashionLandCartPage,
      path: "/cart",
      title: "Cart page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandOurTeam,
      path: "/our-team",
      title: "Our team",
      activeCategory: "other"
    },
    {
      image: images.fashionLandCheckoutPage,
      path: "/checkout",
      title: "Checkout page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandComingSoon,
      path: "/coming-soon",
      title: "Coming soon page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandFag,
      path: "/faqs",
      title: "Faq's page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandForgotPassword,
      path: "/forgot-pwd",
      title: "Forgot passowrd page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandOrderComplete,
      path: "/order-complete",
      title: "Order complete page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandSignup,
      path: "/signup",
      title: "Signup page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandSignin,
      path: "/signin",
      title: "Signin page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandStores,
      path: "/store",
      title: "Store page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandWishlist,
      path: "/wishlist",
      title: "Wishlist page",
      activeCategory: "other"
    },
    {
      image: images.fashionLand4Not,
      path: "/fnf",
      title: "404 page",
      activeCategory: "other"
    },
    {
      image: images.fashionLandFooter1,
      path: "/home1?footerType=footer2",
      title: "Footer page 1",
      activeCategory: "other"
    },
    {
      image: images.fashionLandFooter2,
      path: "/home1?footerType=footer3",
      title: "Footer page 2",
      activeCategory: "other"
    },
    {
      image: images.fashionLandFooter3,
      path: "/home1?footerType=footer4",
      title: "Footer page 3",
      activeCategory: "other"
    }
  ]
};

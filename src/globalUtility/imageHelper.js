/*Index*/
import FashionDemo from "../../public/assets/images/libazz-fashion-land-demo.jpg";
import MobileHomePage1 from "../../public/assets/images/libazz-fashion-mobile-home-page.jpg";
import MobileHomePage2 from "../../public/assets/images/libazz-fashion-mobile-home-page2.jpg";
import MobileHomePage3 from "../../public/assets/images/libazz-fashion-mobile-home-page3.jpg";
import MobileHomePage4 from "../../public/assets/images/libazz-fashion-mobile-home-page4.jpg";
import MobileHomePage5 from "../../public/assets/images/libazz-fashion-mobile-home-page5.jpg";
import MobileHomePage6 from "../../public/assets/images/libazz-fashion-mobile-home-page6.jpg";
import MobileHomePage7 from "../../public/assets/images/libazz-fashion-mobile-home-page7.jpg";
import LandMaster from "../../public/assets/images/libazz-fashion-land-master.jpg";
import LandMailo from "../../public/assets/images/libazz-fashion-land-mailo.jpg";
import LandMacrohtml from "../../public/assets/images/libazz-fashion-land-macrohtml.jpg";
import LandMacroshopify from "../../public/assets/images/libazz-fashion-land-macroshopify.jpg";

import FashionHome1 from "../../public/assets/images/libazz-fashion-home-page.jpg";
import FashionHome2 from "../../public/assets/images/libazz-fashion-home-page2.jpg";
import FashionHome3 from "../../public/assets/images/libazz-fashion-home-page3.jpg";
import FashionHome4 from "../../public/assets/images/libazz-fashion-home-page4.jpg";
import FashionHome5 from "../../public/assets/images/libazz-fashion-home-page5.jpg";
import FashionMenCategory from "../../public/assets/images/libazz-fashion-men-category.jpg";
import FashionWomenCategory from "../../public/assets/images/libazz-fashion-women-category.jpg";
import fashionLandBlog from "../../public/assets/images/libazz-fashion-land-blog.jpg";
import fashionLandBlogLeft from "../../public/assets/images/libazz-fashion-land-blog-left-side.jpg";
import fashionLandBlogRight from "../../public/assets/images/libazz-fashion-land-blog-right-side.jpg";
import fashionLandBlogLeftRight from "../../public/assets/images/libazz-fashion-land-blog-left-right-side.jpg";
import fashionLandBlogGrid from "../../public/assets/images/libazz-fashion-land-blog-2-grid.jpg";
import fashionLandBlogGridLeft from "../../public/assets/images/libazz-fashion-land-blog-2-grid-left-side.jpg";
import fashionLandBlogGridRight from "../../public/assets/images/libazz-fashion-land-blog-2-grid-right-side.jpg";
import fashionLandBlogGrid3 from "../../public/assets/images/libazz-fashion-land-blog-3-grid.jpg";
import fashionLandBlogGrid4 from "../../public/assets/images/libazz-fashion-land-blog-4-grid.jpg";
import fashionLandBlogGridSimple2 from "../../public/assets/images/libazz-fashion-land-blog-2-grid-simple.jpg";
import fashionLandBlogGridSimple3 from "../../public/assets/images/libazz-fashion-land-blog-3-grid-simple.jpg";
import fashionLandBlogGridSimple4 from "../../public/assets/images/libazz-fashion-land-blog-4-grid-simple.jpg";
import fashionLandBlogList from "../../public/assets/images/libazz-fashion-land-blog-list.jpg";
import fashionLandBlogListLeft from "../../public/assets/images/libazz-fashion-land-blog-list-left-side.jpg";
import fashionLandBlogListRight from "../../public/assets/images/libazz-fashion-land-blog-list-right-side.jpg";
import fashionLandBlogListLeftRight from "../../public/assets/images/libazz-fashion-land-blog-list-left-right-side.jpg";
import fashionLandBlogClassic from "../../public/assets/images/libazz-fashion-land-blog-classic.jpg";
import fashionLandBlogLeftSticky from "../../public/assets/images/libazz-fashion-land-blog-left-sticky.jpg";
import fashionLandBlogRightSticky from "../../public/assets/images/libazz-fashion-land-blog-right-sticky.jpg";
import fashionLandBlogLeftRightSticky from "../../public/assets/images/libazz-fashion-land-blog-left-right-sticky.jpg";
import fashionLandPro2grid from "../../public/assets/images/libazz-fashion-land-pro-2-grid.jpg";
import fashionLandPro2GridLeft from "../../public/assets/images/libazz-fashion-land-pro-2-grid-left.jpg";
import fashionLandPro2GridLeftRight from "../../public/assets/images/libazz-fashion-land-pro-2-grid-right.jpg";
import fashionLandPro3Grid from "../../public/assets/images/libazz-fashion-land-pro-3-grid.jpg";
import fashionLandPro4Grid from "../../public/assets/images/libazz-fashion-land-pro-4-grid.jpg";
import fashionLandPro6Grid from "../../public/assets/images/libazz-fashion-land-pro-6-grid.jpg";
import fashionLandAllInOne from "../../public/assets/images/libazz-fashion-land-all-in-one-grid.jpg";
import fashionLandProList from "../../public/assets/images/libazz-fashion-land-pro-list.jpg";
import fashionLandProListLeft from "../../public/assets/images/libazz-fashion-land-pro-list-left-side.jpg";
import fashionLandProListRight from "../../public/assets/images/libazz-fashion-land-pro-list-right-side.jpg";
import fashionLandProListAdv from "../../public/assets/images/libazz-fashion-land-pro-list-adv.jpg";
import fashionLandProDetail from "../../public/assets/images/libazz-fashion-land-pro-detail.jpg";
import fashionLandProDetailSimple from "../../public/assets/images/libazz-fashion-land-pro-detail-simple.jpg";
import fashionLandProDetailSlider from "../../public/assets/images/libazz-fashion-land-pro-detail-slider.jpg";
import fashionLandProDetailSticky from "../../public/assets/images/libazz-fashion-land-pro-detail-sticky.jpg";
import fashionLandProDetailZoom from "../../public/assets/images/libazz-fashion-land-pro-detail-zoom.jpg";
import fashionLandProComparision from "../../public/assets/images/libazz-fashion-land-pro-comparision.jpg";
import fashionLandProComparisionAdv from "../../public/assets/images/libazz-fashion-land-pro-comparision-adv.jpg";
import fashionLandProSearch from "../../public/assets/images/libazz-fashion-land-pro-search.jpg";
import fashionLandProSearch2 from "../../public/assets/images/libazz-fashion-land-pro-search-2.jpg";
import fashionLandAboutUs from "../../public/assets/images/libazz-fashion-land-about-us.jpg";
import fashionLandContactUs from "../../public/assets/images/libazz-fashion-land-contact-us.jpg";
import fashionLandContactUs2 from "../../public/assets/images/libazz-fashion-land-contact-us-2.jpg";
import fashionLandCartPage from "../../public/assets/images/libazz-fashion-land-cart-page.jpg";
import fashionLandOurTeam from "../../public/assets/images/libazz-fashion-land-our-team.jpg";
import fashionLandCheckoutPage from "../../public/assets/images/libazz-fashion-land-checkout-page.jpg";
import fashionLandComingSoon from "../../public/assets/images/libazz-fashion-land-coming-soon.jpg";
import fashionLandFag from "../../public/assets/images/libazz-fashion-land-faq.jpg";
import fashionLandForgotPassword from "../../public/assets/images/libazz-fashion-land-forgot-password.jpg";
import fashionLandOrderComplete from "../../public/assets/images/libazz-fashion-land-order-complete.jpg";
import fashionLandSignup from "../../public/assets/images/libazz-fashion-land-signup.jpg";
import fashionLandSignin from "../../public/assets/images/libazz-fashion-land-signin.jpg";
import fashionLandStores from "../../public/assets/images/libazz-fashion-land-stores.jpg";
import fashionLandWishlist from "../../public/assets/images/libazz-fashion-land-wishlist.jpg";
import fashionLand4Not from "../../public/assets/images/libazz-fashion-land-4-not-4.jpg";
import fashionLandFooter1 from "../../public/assets/images/libazz-fashion-land-footer1.jpg";
import fashionLandFooter2 from "../../public/assets/images/libazz-fashion-land-footer2.jpg";
import fashionLandFooter3 from "../../public/assets/images/libazz-fashion-land-footer3.jpg";

import Bootstrap from "../../public/assets/images/libazz-fashion-bootstrap-icon.png";
import Jquery from "../../public/assets/images/libazz-fashion-jquery-img.png";
import Responsive from "../../public/assets/images/libazz-fashion-responsive.png";
import Customize from "../../public/assets/images/libazz-fashion-customization.png";
import Icons from "../../public/assets/images/libazz-fashion-themify-img.png";
import Variant from "../../public/assets/images/libazz-fashion-swiper-img.png";
import WellDocument from "../../public/assets/images/libazz-fashion-well-document-img.png";
import WellDesign from "../../public/assets/images/libazz-fashion-desing-icon.png";
import VariantFeature from "../../public/assets/images/libazz-fashion-feature-img.png";
import Masonary from "../../public/assets/images/libazz-fashion-masonary.png";
import VariousGrid from "../../public/assets/images/libazz-fashion-grid.png";
import VariousList from "../../public/assets/images/libazz-fashion-list-land.png";
import Timer from "../../public/assets/images/libazz-fashion-deal-timer-img.png";
import Validate from "../../public/assets/images/libazz-fashion-validate-img.png";
import Instafeed from "../../public/assets/images/libazz-fashion-instafeed-img.png";
import GoogleFont from "../../public/assets/images/libazz-fashion-google-font.png";
import GoogleMap from "../../public/assets/images/libazz-fashion-google-map.png";
import GoogleMap1 from "../../public/assets/images/libazz-fashion-seo.png";
/*header Image*/
import FashionPopUp from "../../public/assets/images/libazz-fashion-pop-up.jpg";

import LibazzLogo from "../../public/assets/images/libazz-fashion-logo.png";
import FashionCart1 from "../../public/assets/images/libazz-fashion-cart-1.jpg";
import FashionCart2 from "../../public/assets/images/libazz-fashion-cart-2.jpg";
import FashionCart3 from "../../public/assets/images/libazz-fashion-cart-3.jpg";
import FashionCart4 from "../../public/assets/images/libazz-fashion-cart-4.jpg";
import FashionCart5 from "../../public/assets/images/libazz-fashion-cart-5.jpg";
import FashionMegaMenu from "../../public/assets/images/libazz-fashion-megamenu-img.jpg";

/*home Layout 2*/
import Home2PopUp from "../../public/assets/images/libazz-fashion-pop-up-2.jpg";
import Home2MegaMenu1 from "../../public/assets/images/libazz-fashion-megamenu-img1.jpg";
import Home2MegaMenu3 from "../../public/assets/images/libazz-fashion-megamenu-img3.jpg";
import Home2Slider1 from "../../public/assets/images/libazz-fashion-sliderimage4.jpg";
import Home2Slider2 from "../../public/assets/images/libazz-fashion-sliderimage5.jpg";
import Home2Slider3 from "../../public/assets/images/libazz-fashion-sliderimage6.jpg";
import Home2Product1 from "../../public/assets/images/libazz-fashion-product5.jpg";
import Home2Product2 from "../../public/assets/images/libazz-fashion-product6.jpg";
import Home2Product3 from "../../public/assets/images/libazz-fashion-product4.jpg";
import Home2Trending1 from "../../public/assets/images/libazz-fashion-recent-product.jpg";
import Home2Trending2 from "../../public/assets/images/libazz-fashion-t1.jpg";
import Home2Trending3 from "../../public/assets/images/libazz-fashion-t2.jpg";
import Home2Trending4 from "../../public/assets/images/libazz-fashion-t3.jpg";
import Home2Deal1 from "../../public/assets/images/libazz-fashion-dealday3.jpg";
import Home2Deal2 from "../../public/assets/images/libazz-fashion-dealweek3.jpg";
import Home2Deal3 from "../../public/assets/images/libazz-fashion-unlimited.jpg";
import Home2Footer1 from "../../public/assets/images/libazz-fashion-footer-bg1.jpg";
import Home2Footer2 from "../../public/assets/images/libazz-fashion-footer-bg2.jpg";
import Home2Footer3 from "../../public/assets/images/libazz-fashion-footer-bg3.jpg";
import Home2Footer4 from "../../public/assets/images/libazz-fashion-footer-bg4.jpg";

/*home Layout 3*/
import Home3Popup from "../../public/assets/images/libazz-fashion-pop-up-3.jpg";
import FashionMegaMenu2 from "../../public/assets/images/libazz-fashion-megamenu-img2.jpg";
import Home3Slider1 from "../../public/assets/images/libazz-fashion-sliderimage7.png";
import Home3Slider2 from "../../public/assets/images/libazz-fashion-sliderimage8.png";
import Home3Slider3 from "../../public/assets/images/libazz-fashion-sliderimage9.png";
import Home3Category1 from "../../public/assets/images/tshirt.png";
import Home3Category2 from "../../public/assets/images/shoes.png";
import Home3Category3 from "../../public/assets/images/dress.png";
import Home3Category4 from "../../public/assets/images/jacket.png";
import Home3Category5 from "../../public/assets/images/watch.png";
import Home3Category6 from "../../public/assets/images/shorts.png";
import Home3Category7 from "../../public/assets/images/bikini.png";
import Home3Category8 from "../../public/assets/images/bag.png";
import Home3Category9 from "../../public/assets/images/sunglasses.png";
import Home3Category10 from "../../public/assets/images/purse.png";
import Home3Product19 from "../../public/assets/images/libazz-fashion-productimg19.jpg";
import Home3Product1919 from "../../public/assets/images/libazz-fashion-productimg19-19.jpg";
import Home3Product20 from "../../public/assets/images/libazz-fashion-productimg20.jpg";
import Home3Product2020 from "../../public/assets/images/libazz-fashion-productimg20-20.jpg";
import Home3Product21 from "../../public/assets/images/libazz-fashion-productimg21.jpg";
import Home3Product2121 from "../../public/assets/images/libazz-fashion-productimg21-21.jpg";
import Home3Product18 from "../../public/assets/images/libazz-fashion-productimg18.jpg";
import Home3Product1818 from "../../public/assets/images/libazz-fashion-productimg18-18.jpg";
import Home3Comment1 from "../../public/assets/images/libazz-fashion-parallax2.jpg";

/*home Layout 4*/
import Home4Slider1 from "../../public/assets/images/libazz-fashion-sliderimage7.jpg";
import Home4Slider2 from "../../public/assets/images/libazz-fashion-sliderimage8.jpg";
import Home4Slider3 from "../../public/assets/images/libazz-fashion-sliderimage9.jpg";

/*BestDeal*/
import FashionBestDeal1 from "../../public/assets/images/libazz-fashion-bestdeal1.jpg";
import FashionBestDeal2 from "../../public/assets/images/libazz-fashion-bestdeal2.jpg";
import FashionBestDeal3 from "../../public/assets/images/libazz-fashion-bestdeal3.jpg";
import FashionBestDeal4 from "../../public/assets/images/libazz-fashion-bestdeal4.jpg";
import FashionBestDeal5 from "../../public/assets/images/libazz-fashion-bestdeal5.jpg";
import FashionBestDeal6 from "../../public/assets/images/libazz-fashion-bestdeal6.jpg";

/*Slider*/
import FashionSliderImage1 from "../../public/assets/images/libazz-fashion-sliderimage1.jpg";
import FashionSliderImage2 from "../../public/assets/images/libazz-fashion-sliderimage2.jpg";
import FashionSliderImage3 from "../../public/assets/images/libazz-fashion-sliderimage3.jpg";
import FashionProduct1 from "../../public/assets/images/libazz-fashion-product1.jpg";
import FashionProduct2 from "../../public/assets/images/libazz-fashion-product2.jpg";
import FashionProduct3 from "../../public/assets/images/libazz-fashion-product3.jpg";

/*Deals*/
import FashionDealDay1 from "../../public/assets/images/libazz-fashion-dealday1.jpg";
import FashionDealDay2 from "../../public/assets/images/libazz-fashion-dealday2.jpg";
import FashionDealWeek1 from "../../public/assets/images/libazz-fashion-dealweek1.jpg";
import FashionDealWeek2 from "../../public/assets/images/libazz-fashion-dealweek2.jpg";
/*FollowSupport*/
import FashionInsta from "../../public/assets/images/libazz-fashion-insta.png";
import FashionFacebook from "../../public/assets/images/libazz-fashion-facebook.png";

/*footer*/
import FooterLogo from "../../public/assets/images/libazz-fashion-footer-logo.png";
import FooterPayment from "../../public/assets/images/libazz-fashion-payment.png";

/*OurProducts*/
import FashionProductImg1 from "../../public/assets/images/libazz-fashion-productimg1.jpg";
import FashionProductImg12 from "../../public/assets/images/libazz-fashion-productimg1-2.jpg";
import FashionProductImg2 from "../../public/assets/images/libazz-fashion-productimg2.jpg";
import FashionProductImg22 from "../../public/assets/images/libazz-fashion-productimg2-2.jpg";
import FashionProductImg3 from "../../public/assets/images/libazz-fashion-productimg3.jpg";
import FashionProductImg33 from "../../public/assets/images/libazz-fashion-productimg3-3.jpg";
import FashionProductSideImg1 from "../../public/assets/images/libazz-fashion-productsideimg1.jpg";
import FashionProductSideImg2 from "../../public/assets/images/libazz-fashion-productsideimg2.jpg";
import FashionProductImg4 from "../../public/assets/images/libazz-fashion-productimg4.jpg";
import FashionProductImg44 from "../../public/assets/images/libazz-fashion-productimg4-4.jpg";
import FashionProductImg5 from "../../public/assets/images/libazz-fashion-productimg5.jpg";
import FashionProductImg55 from "../../public/assets/images/libazz-fashion-productimg5-5.jpg";
import FashionProductImg6 from "../../public/assets/images/libazz-fashion-productimg6.jpg";
import FashionProductImg66 from "../../public/assets/images/libazz-fashion-productimg6-6.jpg";
import FashionProductImg7 from "../../public/assets/images/libazz-fashion-productimg7.jpg";
import FashionProductImg8 from "../../public/assets/images/libazz-fashion-productimg8.jpg";
import FashionProductImg9 from "../../public/assets/images/libazz-fashion-productimg9.jpg";
import FashionProductImg10 from "../../public/assets/images/libazz-fashion-productimg10.jpg";
import FashionProductImg11 from "../../public/assets/images/libazz-fashion-productimg11.jpg";
import FashionProductImg12A from "../../public/assets/images/libazz-fashion-productimg12.jpg";
import FashionProductImg13 from "../../public/assets/images/libazz-fashion-productimg13.jpg";
import FashionProductImg14 from "../../public/assets/images/libazz-fashion-productimg14.jpg";
import FashionProductImg15 from "../../public/assets/images/libazz-fashion-productimg15.jpg";
import FashionProductImg16 from "../../public/assets/images/libazz-fashion-productimg16.jpg";
import FashionProductImg1616 from "../../public/assets/images/libazz-fashion-productimg16-16.jpg";
import FashionProductImg17 from "../../public/assets/images/libazz-fashion-productimg17.jpg";
import FashionProductImg1717 from "../../public/assets/images/libazz-fashion-productimg17-17.jpg";
import FashionProductImg24 from "../../public/assets/images/libazz-fashion-productimg24.jpg";
import FashionProductImg2424 from "../../public/assets/images/libazz-fashion-productimg24-24.jpg";
import FashionPurse3 from "../../public/assets/images/libazz-fashion-purse3.jpg";
import FashionShoes1 from "../../public/assets/images/libazz-fashion-shoes1.jpg";
import FashionShoes2 from "../../public/assets/images/libazz-fashion-shoes2.jpg";

/*blog*/
import FashionBlog1Img from "../../public/assets/images/libazz-fashion-blog1-img.jpg";
import FashionUserImg from "../../public/assets/images/libazz-fashion-user-img.jpg";
import FashionPrev from "../../public/assets/images/libazz-fashion-prev.jpg";
import FashionNext from "../../public/assets/images/libazz-fashion-next.jpg";
import FashionCl from "../../public/assets/images/libazz-fashion-c1.jpg";
import FashionC2 from "../../public/assets/images/libazz-fashion-c2.jpg";
import FashionC3 from "../../public/assets/images/libazz-fashion-c3.jpg";
import FashionC4 from "../../public/assets/images/libazz-fashion-c4.jpg";
import FashionC5 from "../../public/assets/images/libazz-fashion-c5.jpg";

import FashionRecomBlog1 from "../../public/assets/images/libazz-fashion-recom-blog1.jpg";
import FashionRecomBlog2 from "../../public/assets/images/libazz-fashion-recom-blog2.jpg";
import FashionBlog1 from "../../public/assets/images/libazz-fashion-blog-1.jpg";
import FashionBlog2 from "../../public/assets/images/libazz-fashion-blog-2.jpg";
import FashionBlog3 from "../../public/assets/images/libazz-fashion-blog-3.jpg";
import FashionBlog4 from "../../public/assets/images/libazz-fashion-blog-4.jpg";
import FashionBlog5 from "../../public/assets/images/libazz-fashion-blog-5.jpg";
import FashionBlog6 from "../../public/assets/images/libazz-fashion-blog-6.jpg";

import FashionBlogGrid24 from "../../public/assets/images/libazz-fashion-blog-grid24.jpg";
import FashionBlogGrid23 from "../../public/assets/images/libazz-fashion-blog-grid23.jpg";
import FashionBlogGrid22 from "../../public/assets/images/libazz-fashion-blog-grid22.jpg";
import FashionBlogGrid21 from "../../public/assets/images/libazz-fashion-blog-grid21.jpg";
import FashionBlogGrid20 from "../../public/assets/images/libazz-fashion-blog-grid20.jpg";
import FashionBlogGrid19 from "../../public/assets/images/libazz-fashion-blog-grid19.jpg";
import FashionBlogGrid18 from "../../public/assets/images/libazz-fashion-blog-grid18.jpg";
import FashionBlogGrid17 from "../../public/assets/images/libazz-fashion-blog-grid17.jpg";
import FashionBlogGrid16 from "../../public/assets/images/libazz-fashion-blog-grid16.jpg";
import FashionBlogGrid15 from "../../public/assets/images/libazz-fashion-blog-grid15.jpg";
import FashionBlogGrid14 from "../../public/assets/images/libazz-fashion-blog-grid14.jpg";
import FashionBlogGrid13 from "../../public/assets/images/libazz-fashion-blog-grid13.jpg";
import FashionBlogGrid12 from "../../public/assets/images/libazz-fashion-blog-grid12.jpg";
import FashionBlogGrid11 from "../../public/assets/images/libazz-fashion-blog-grid11.jpg";
import FashionBlogGrid10 from "../../public/assets/images/libazz-fashion-blog-grid10.jpg";
import FashionBlogGrid9 from "../../public/assets/images/libazz-fashion-blog-grid9.jpg";
import FashionBlogGrid8 from "../../public/assets/images/libazz-fashion-blog-grid8.jpg";
import FashionBlogGrid7 from "../../public/assets/images/libazz-fashion-blog-grid7.jpg";
import FashionBlogGrid6 from "../../public/assets/images/libazz-fashion-blog-grid6.jpg";
import FashionBlogGrid5 from "../../public/assets/images/libazz-fashion-blog-grid5.jpg";
import FashionBlogGrid4 from "../../public/assets/images/libazz-fashion-blog-grid4.jpg";
import FashionBlogGrid3 from "../../public/assets/images/libazz-fashion-blog-grid3.jpg";
import FashionBlogGrid2 from "../../public/assets/images/libazz-fashion-blog-grid2.jpg";
import FashionBlogGrid1 from "../../public/assets/images/libazz-fashion-blog-grid1.jpg";

import FashionBlogList1 from "../../public/assets/images/libazz-fashion-blog-list1.jpg";
import FashionBlogList2 from "../../public/assets/images/libazz-fashion-blog-list2.jpg";
import FashionBlogList3 from "../../public/assets/images/libazz-fashion-blog-list3.jpg";
import FashionBlogList4 from "../../public/assets/images/libazz-fashion-blog-list4.jpg";
import FashionBlogList5 from "../../public/assets/images/libazz-fashion-blog-list5.jpg";
import FashionBlogList6 from "../../public/assets/images/libazz-fashion-blog-list6.jpg";
import FashionBlogList7 from "../../public/assets/images/libazz-fashion-blog-list7.jpg";
import FashionBlogList8 from "../../public/assets/images/libazz-fashion-blog-list8.jpg";

import FashionBlogClassic1 from "../../public/assets/images/libazz-fashion-blog-classic1.jpg";
import FashionBlogClassic2 from "../../public/assets/images/libazz-fashion-blog-classic2.jpg";
import FashionBlogClassic3 from "../../public/assets/images/libazz-fashion-blog-classic3.jpg";
import FashionBlogClassic4 from "../../public/assets/images/libazz-fashion-blog-classic4.jpg";
import FashionBlogClassic5 from "../../public/assets/images/libazz-fashion-blog-classic5.jpg";

/*Category*/
import CategoryGrid from "../../public/assets/images/libazz-fashion-4-grid.png";
import CategoryGridWhite from "../../public/assets/images/libazz-fashion-4-grid-white.png";
import CategoryFashionList from "../../public/assets/images/libazz-fashion-list.png";
import CategoryFashionListWhite from "../../public/assets/images/libazz-fashion-list-white.png";
import CategoryProduct from "../../public/assets/images/libazz-fashion-product-sideimg1.jpg";
import CategoryTShirt1 from "../../public/assets/images/libazz-fashion-tshirt1.jpg";
import CategoryTShirt2 from "../../public/assets/images/libazz-fashion-tshirt2.jpg";
import CategoryTShirt3 from "../../public/assets/images/libazz-fashion-tshirt3.jpg";
import CategoryTShirt4 from "../../public/assets/images/libazz-fashion-tshirt4.jpg";
import CategoryTShirt5 from "../../public/assets/images/libazz-fashion-tshirt5.jpg";
import CategoryTShirt6 from "../../public/assets/images/libazz-fashion-tshirt6.jpg";
import CategoryTShirt7 from "../../public/assets/images/libazz-fashion-tshirt7.jpg";
import CategoryTShirt8 from "../../public/assets/images/libazz-fashion-tshirt8.jpg";
import CategoryShirt1 from "../../public/assets/images/libazz-fashion-shirt1.jpg";
import CategoryShirt2 from "../../public/assets/images/libazz-fashion-shirt2.jpg";
import CategoryShirt3 from "../../public/assets/images/libazz-fashion-shirt3.jpg";
import CategoryShirt4 from "../../public/assets/images/libazz-fashion-shirt4.jpg";
import CategoryShirt5 from "../../public/assets/images/libazz-fashion-shirt5.jpg";
import CategoryShirt6 from "../../public/assets/images/libazz-fashion-shirt6.jpg";
import CategoryShirt7 from "../../public/assets/images/libazz-fashion-shirt7.jpg";
import CategoryShirt8 from "../../public/assets/images/libazz-fashion-shirt8.jpg";
import CategoryJeans1 from "../../public/assets/images/libazz-fashion-jeans1.jpg";
import CategoryJeans2 from "../../public/assets/images/libazz-fashion-jeans2.jpg";
import CategoryJeans3 from "../../public/assets/images/libazz-fashion-jeans3.jpg";
import CategoryJeans4 from "../../public/assets/images/libazz-fashion-jeans4.jpg";
import CategoryJeans5 from "../../public/assets/images/libazz-fashion-jeans5.jpg";
import CategoryJeans6 from "../../public/assets/images/libazz-fashion-jeans6.jpg";
import CategoryJeans7 from "../../public/assets/images/libazz-fashion-jeans7.jpg";
import CategoryJeans8 from "../../public/assets/images/libazz-fashion-jeans8.jpg";
import CategoryJacket1 from "../../public/assets/images/libazz-fashion-jacket1.jpg";
import CategoryJacket2 from "../../public/assets/images/libazz-fashion-jacket2.jpg";
import CategoryJacket3 from "../../public/assets/images/libazz-fashion-jacket3.jpg";
import CategoryJacket4 from "../../public/assets/images/libazz-fashion-jacket4.jpg";
import CategoryJacket5 from "../../public/assets/images/libazz-fashion-jacket5.jpg";
import CategoryJacket6 from "../../public/assets/images/libazz-fashion-jacket6.jpg";
import CategoryJacket7 from "../../public/assets/images/libazz-fashion-jacket7.jpg";
import CategoryJacket8 from "../../public/assets/images/libazz-fashion-jacket8.jpg";
import CategoryShoes2 from "../../public/assets/images/libazz-fashion-shoes2.jpg";
import CategoryShoes3 from "../../public/assets/images/libazz-fashion-shoes3.jpg";
import CategoryShoes4 from "../../public/assets/images/libazz-fashion-shoes4.jpg";
import CategoryShoes5 from "../../public/assets/images/libazz-fashion-shoes5.jpg";
import CategoryShoes6 from "../../public/assets/images/libazz-fashion-shoes6.jpg";
import CategoryShoes7 from "../../public/assets/images/libazz-fashion-shoes7.jpg";
import CategoryShoes8 from "../../public/assets/images/libazz-fashion-shoes8.jpg";
import CategoryBag1 from "../../public/assets/images/libazz-fashion-bag1.jpg";
import CategoryBag2 from "../../public/assets/images/libazz-fashion-bag2.jpg";
import CategoryBag3 from "../../public/assets/images/libazz-fashion-bag3.jpg";
import CategoryBag4 from "../../public/assets/images/libazz-fashion-bag4.jpg";
import CategoryGlasses1 from "../../public/assets/images/libazz-fashion-glasses1.jpg";
import CategoryGlasses2 from "../../public/assets/images/libazz-fashion-glasses2.jpg";
import CategoryGlasses3 from "../../public/assets/images/libazz-fashion-glasses3.jpg";
import CategoryGlasses4 from "../../public/assets/images/libazz-fashion-glasses4.jpg";
import CategoryGlasses5 from "../../public/assets/images/libazz-fashion-glasses5.jpg";
import CategoryGlasses6 from "../../public/assets/images/libazz-fashion-glasses6.jpg";
import CategoryGlasses7 from "../../public/assets/images/libazz-fashion-glasses7.jpg";
import CategoryGlasses8 from "../../public/assets/images/libazz-fashion-glasses8.jpg";
import CategoryWomenTShirt1 from "../../public/assets/images/libazz-fashion-top1.jpg";
import CategoryWomenTShirt2 from "../../public/assets/images/libazz-fashion-top2.jpg";
import CategoryWomenTShirt3 from "../../public/assets/images/libazz-fashion-top3.jpg";
import CategoryWomenTShirt4 from "../../public/assets/images/libazz-fashion-top4.jpg";
import CategoryWomenTShirt5 from "../../public/assets/images/libazz-fashion-top5.jpg";
import CategoryWomenTShirt6 from "../../public/assets/images/libazz-fashion-top6.jpg";
import CategoryWomenTShirt7 from "../../public/assets/images/libazz-fashion-top7.jpg";
import CategoryWomenTShirt8 from "../../public/assets/images/libazz-fashion-top8.jpg";
import CategoryWomenShirt1 from "../../public/assets/images/libazz-fashion-w-shirt1.jpg";
import CategoryWomenShirt2 from "../../public/assets/images/libazz-fashion-w-shirt2.jpg";
import CategoryWomenShirt3 from "../../public/assets/images/libazz-fashion-w-shirt3.jpg";
import CategoryWomenShirt4 from "../../public/assets/images/libazz-fashion-w-shirt4.jpg";

import CategoryWomenShirt5 from "../../public/assets/images/libazz-fashion-w-shirt5.jpg";
import CategoryWomenShirt6 from "../../public/assets/images/libazz-fashion-w-shirt6.jpg";
import CategoryWomenShirt7 from "../../public/assets/images/libazz-fashion-w-shirt7.jpg";
import CategoryWomenShirt8 from "../../public/assets/images/libazz-fashion-w-shirt8.jpg";
import CategoryWomenJeans1 from "../../public/assets/images/libazz-fashion-w-jeans1.jpg";
import CategoryWomenJeans2 from "../../public/assets/images/libazz-fashion-w-jeans2.jpg";
import CategoryWomenJeans3 from "../../public/assets/images/libazz-fashion-w-jeans3.jpg";
import CategoryWomenJeans4 from "../../public/assets/images/libazz-fashion-w-jeans4.jpg";
import CategoryWomenJeans5 from "../../public/assets/images/libazz-fashion-w-jeans5.jpg";
import CategoryWomenJeans6 from "../../public/assets/images/libazz-fashion-w-jeans6.jpg";
import CategoryWomenJeans7 from "../../public/assets/images/libazz-fashion-w-jeans7.jpg";
import CategoryWomenJeans8 from "../../public/assets/images/libazz-fashion-w-jeans8.jpg";
import CategoryWomenShoes2 from "../../public/assets/images/libazz-fashion-shoes2.jpg";
import CategoryWomenShoes4 from "../../public/assets/images/libazz-fashion-shoes4.jpg";
import CategoryWomenShoes5 from "../../public/assets/images/libazz-fashion-shoes5.jpg";
import CategoryWomenShoes6 from "../../public/assets/images/libazz-fashion-shoes6.jpg";
import CategoryWomenLeggings1 from "../../public/assets/images/libazz-fashion-lengirie1.jpg";
import CategoryWomenLeggings2 from "../../public/assets/images/libazz-fashion-lengirie2.jpg";
import CategoryWomenLeggings3 from "../../public/assets/images/libazz-fashion-lengirie3.jpg";
import CategoryWomenLeggings4 from "../../public/assets/images/libazz-fashion-lengirie4.jpg";
import CategoryWomenLeggings5 from "../../public/assets/images/libazz-fashion-lengirie5.jpg";
import CategoryWomenLeggings6 from "../../public/assets/images/libazz-fashion-lengirie6.jpg";
import CategoryWomenLeggings7 from "../../public/assets/images/libazz-fashion-lengirie7.jpg";
import CategoryWomenLeggings8 from "../../public/assets/images/libazz-fashion-lengirie8.jpg";
import CategoryWomenPurse1 from "../../public/assets/images/libazz-fashion-purse1.jpg";
import CategoryWomenPurse2 from "../../public/assets/images/libazz-fashion-purse2.jpg";
import CategoryWomenPurse3 from "../../public/assets/images/libazz-fashion-purse3.jpg";
import CategoryWomenPurse4 from "../../public/assets/images/libazz-fashion-purse4.jpg";
import CategoryWomenPurse5 from "../../public/assets/images/libazz-fashion-purse5.jpg";
import CategoryWomenPurse8 from "../../public/assets/images/libazz-fashion-productimg8-8.jpg";

/*wishlist*/
import WishList1 from "../../public/assets/images/libazz-fashion-product-1.jpg";
import WishList2 from "../../public/assets/images/libazz-fashion-product-2.jpg";

// common image
import LibazzList2 from "../../public/assets/images/libazz-fashion-list-2.png";
import LibazzList2White from "../../public/assets/images/libazz-fashion-list-2-white.png";
import Fashion3Grid from "../../public/assets/images/libazz-fashion-3-grid.png";
import Fashion3GridWhite from "../../public/assets/images/libazz-fashion-3-grid-white.png";
import Fashion2Grid from "../../public/assets/images/libazz-fashion-2-grid.png";
import Fashion2White from "../../public/assets/images/libazz-fashion-2-grid-white.png";
import Fashion6Grid from "../../public/assets/images/libazz-fashion-6-grid.png";
import Fashion6GridWhite from "../../public/assets/images/libazz-fashion-6-grid-white.png";

/*About Us*/
import About from "../../public/assets/images/libazz-fashion-about-us.jpg";
import Magento from "../../public/assets/images/libazz-fashion-magento.png";
import PrestaShop from "../../public/assets/images/libazz-fashion-prestashop.png";
import Shopify from "../../public/assets/images/libazz-fashion-shopify-bag.png";
import WooCommerce from "../../public/assets/images/libazz-fashion-woo-commerce.png";
import Wordpress from "../../public/assets/images/libazz-fashion-wordpress.png";
import OpenCart from "../../public/assets/images/libazz-fashion-open-cart.png";
import FashionParallax from "../../public/assets/images/libazz-fashion-parallax.jpg";
import Tech1 from "../../public/assets/images/libazz-fashion-spacing-tech.png";
import Tech2 from "../../public/assets/images/libazz-fashion-spacing-tech2.png";
import Tech3 from "../../public/assets/images/libazz-fashion-spacing-tech3.png";
import Tech9 from "../../public/assets/images/libazz-fashion-spacing-tech9.png";
import Tech5 from "../../public/assets/images/libazz-fashion-spacing-tech5.png";
import Tech6 from "../../public/assets/images/libazz-fashion-spacing-tech6.png";
import Tech7 from "../../public/assets/images/libazz-fashion-spacing-tech7.png";
import Tech8 from "../../public/assets/images/libazz-fashion-spacing-tech8.png";

/*Coming Soon*/
import ComingSoon1 from "../../public/assets/images/libazz-fashion-bg01.jpg";
import ComingSoon2 from "../../public/assets/images/libazz-fashion-bg02.jpg";
import ComingSoon3 from "../../public/assets/images/libazz-fashion-bg03.jpg";
import ComingSoon4 from "../../public/assets/images/libazz-fashion-bg04.jpg";

/*Faqs*/
import Faq from "../../public/assets/images/libazz-fashion-faq.jpg";
import EmptySearch from "../../public/assets/images/empty-search.jpg";

/*Sign In, Sign Up*/
import SignUp from "../../public/assets/images/libazz-fashion-sign-up.jpg";
import SignIn from "../../public/assets/images/libazz-fashion-login-img.jpg";

/*store*/
import Store1 from "../../public/assets/images/libazz-fashion-store-1.jpg";
import Store2 from "../../public/assets/images/libazz-fashion-store-2.jpg";
import Store3 from "../../public/assets/images/libazz-fashion-store-3.jpg";
import Store4 from "../../public/assets/images/libazz-fashion-store-4.jpg";
import Store5 from "../../public/assets/images/libazz-fashion-store-5.jpg";

/*contact-us*/
import ContactUs from "../../public/assets/images/libazz-fashion-contact-us.jpg";
import SupportTeam from "../../public/assets/images/libazz-fashion-support-team.jpg";
import BillingTeam from "../../public/assets/images/libazz-fashion-billing-team.jpg";
import EvaluationsTeam from "../../public/assets/images/libazz-fashion-evaluations-team.jpg";

/*Product*/
import FashionRecom1 from "../../public/assets/images/libazz-fashion-recom-1.jpg";
import FashionRecom2 from "../../public/assets/images/libazz-fashion-recom-2.jpg";
import FashionProductimg2323 from "../../public/assets/images/libazz-fashion-productimg23-23.jpg";
import FashionProductimg23 from "../../public/assets/images/libazz-fashion-productimg23.jpg";
import FashionProductimg22 from "../../public/assets/images/libazz-fashion-productimg22.jpg";
import FashionProductimg2222 from "../../public/assets/images/libazz-fashion-productimg22-22.jpg";
import FashionProductimg21 from "../../public/assets/images/libazz-fashion-productimg21.jpg";
import FashionProductimg2121 from "../../public/assets/images/libazz-fashion-productimg21-21.jpg";
import FashionProductimg20 from "../../public/assets/images/libazz-fashion-productimg20.jpg";
import FashionProductimg2020 from "../../public/assets/images/libazz-fashion-productimg20-20.jpg";
import FashionProductimg19 from "../../public/assets/images/libazz-fashion-productimg19.jpg";
import FashionProductimg1919 from "../../public/assets/images/libazz-fashion-productimg19-19.jpg";
import FashionProductimg18 from "../../public/assets/images/libazz-fashion-productimg18.jpg";
import FashionProductimg1818 from "../../public/assets/images/libazz-fashion-productimg18-18.jpg";

/*layout 5*/
import AppleIcon from "../../public/assets/images/apple.png";
import AndroidIcon from "../../public/assets/images/android.png";
import WindowsIcon from "../../public/assets/images/windows.png";
import FashionCbanner1 from "../../public/assets/images/libazz-fashion-cbanner1.png";
import FashionCbanner2 from "../../public/assets/images/libazz-fashion-cbanner2.png";
import FashionSlidebarImg1 from "../../public/assets/images/libazz-fashion-slidebar-img1.jpg";
import FashionSlidebar10 from "../../public/assets/images/libazz-fashion-sliderimage10.jpg";
import FashionSlidebar11 from "../../public/assets/images/libazz-fashion-sliderimage11.jpg";
import FashionSlidebar12 from "../../public/assets/images/libazz-fashion-sliderimage12.jpg";
import FashionBannerProduct1 from "../../public/assets/images/libazz-fashion-banner-product1.jpg";
import FashionBannerProduct2 from "../../public/assets/images/libazz-fashion-banner-product2.jpg";
import FashionBannerProduct3 from "../../public/assets/images/libazz-fashion-banner-product3.jpg";
import FashionBannerProduct4 from "../../public/assets/images/libazz-fashion-banner-product4.jpg";

/*our-team*/
import FashionCeo from "../../public/assets/images/libazz-fashion-ceo.jpg";
import FashionFounder from "../../public/assets/images/libazz-fashion-founder.jpg";
import FashionDesigner from "../../public/assets/images/libazz-fashion-designer.jpg";

/*layout 6*/
import FashionHome6 from "../../public/assets/images/libazz-fashion-home-page6.jpg";

/*layout 7*/
import FashionHome7 from "../../public/assets/images/libazz-fashion-home-page7.jpg";
import SlidebarImg3 from "../../public/assets/images/libazz-fashion-slidebar-img3.jpg";
import BannerProduct5 from "../../public/assets/images/libazz-fashion-banner-product5.jpg";
import BannerProduct6 from "../../public/assets/images/libazz-fashion-banner-product6.jpg";
import SlidebarImg2 from "../../public/assets/images/libazz-fashion-slidebar-img2.jpg";

export const imageUtil = {
  images: {
    FashionDemo: FashionDemo,
    FashionHome1: FashionHome1,
    FashionHome2: FashionHome2,
    FashionHome3: FashionHome3,
    FashionHome4: FashionHome4,
    FashionHome5: FashionHome5,
    FashionHome6: FashionHome6,
    FashionHome7: FashionHome7,
    FashionMenCategory: FashionMenCategory,
    FashionWomenCategory: FashionWomenCategory,
    fashionLandBlog: fashionLandBlog,
    fashionLandBlogLeft: fashionLandBlogLeft,
    fashionLandBlogRight: fashionLandBlogRight,
    fashionLandBlogLeftRight: fashionLandBlogLeftRight,
    fashionLandBlogGrid: fashionLandBlogGrid,
    fashionLandBlogGridLeft: fashionLandBlogGridLeft,
    fashionLandBlogGridRight: fashionLandBlogGridRight,
    fashionLandBlogGrid3: fashionLandBlogGrid3,
    fashionLandBlogGrid4: fashionLandBlogGrid4,
    fashionLandBlogGridSimple2: fashionLandBlogGridSimple2,
    fashionLandBlogGridSimple3: fashionLandBlogGridSimple3,
    fashionLandBlogGridSimple4: fashionLandBlogGridSimple4,
    fashionLandBlogList: fashionLandBlogList,
    fashionLandBlogListLeft: fashionLandBlogListLeft,
    fashionLandBlogListRight: fashionLandBlogListRight,
    fashionLandBlogListLeftRight: fashionLandBlogListLeftRight,
    fashionLandBlogClassic: fashionLandBlogClassic,
    fashionLandBlogLeftSticky: fashionLandBlogLeftSticky,
    fashionLandBlogRightSticky: fashionLandBlogRightSticky,
    fashionLandBlogLeftRightSticky: fashionLandBlogLeftRightSticky,
    fashionLandPro2grid: fashionLandPro2grid,
    fashionLandPro2GridLeft: fashionLandPro2GridLeft,
    fashionLandPro2GridLeftRight: fashionLandPro2GridLeftRight,
    fashionLandPro3Grid: fashionLandPro3Grid,
    fashionLandPro4Grid: fashionLandPro4Grid,
    fashionLandPro6Grid: fashionLandPro6Grid,
    fashionLandAllInOne: fashionLandAllInOne,
    fashionLandProList: fashionLandProList,
    fashionLandProListLeft: fashionLandProListLeft,
    fashionLandProListRight: fashionLandProListRight,
    fashionLandProListAdv: fashionLandProListAdv,
    fashionLandProDetail: fashionLandProDetail,
    fashionLandProDetailSimple: fashionLandProDetailSimple,
    fashionLandProDetailSlider: fashionLandProDetailSlider,
    fashionLandProDetailSticky: fashionLandProDetailSticky,
    fashionLandProDetailZoom: fashionLandProDetailZoom,
    fashionLandProComparision: fashionLandProComparision,
    fashionLandProComparisionAdv: fashionLandProComparisionAdv,
    fashionLandProSearch: fashionLandProSearch,
    fashionLandProSearch2: fashionLandProSearch2,
    fashionLandAboutUs: fashionLandAboutUs,
    fashionLandContactUs: fashionLandContactUs,
    fashionLandContactUs2: fashionLandContactUs2,
    fashionLandCartPage: fashionLandCartPage,
    fashionLandOurTeam: fashionLandOurTeam,
    fashionLandCheckoutPage: fashionLandCheckoutPage,
    fashionLandComingSoon: fashionLandComingSoon,
    fashionLandFag: fashionLandFag,
    fashionLandForgotPassword: fashionLandForgotPassword,
    fashionLandOrderComplete: fashionLandOrderComplete,
    fashionLandSignup: fashionLandSignup,
    fashionLandSignin: fashionLandSignin,
    fashionLandStores: fashionLandStores,
    fashionLandWishlist: fashionLandWishlist,
    fashionLand4Not: fashionLand4Not,
    fashionLandFooter1: fashionLandFooter1,
    fashionLandFooter2: fashionLandFooter2,
    fashionLandFooter3: fashionLandFooter3,
    Bootstrap: Bootstrap,
    Jquery: Jquery,
    Responsive: Responsive,
    Customize: Customize,
    Icons: Icons,
    Variant: Variant,
    WellDocument: WellDocument,
    WellDesign: WellDesign,
    VariantFeature: VariantFeature,
    Masonary: Masonary,
    VariousGrid: VariousGrid,
    VariousList: VariousList,
    Timer: Timer,
    Validate: Validate,
    Instafeed: Instafeed,
    GoogleFont: GoogleFont,
    GoogleMap: GoogleMap,
    GoogleMap1: GoogleMap1,
    FashionPopUp: FashionPopUp,
    LibazzLogo: LibazzLogo,
    FashionCart1: FashionCart1,
    FashionCart2: FashionCart2,
    FashionCart3: FashionCart3,
    FashionCart4: FashionCart4,
    FashionCart5: FashionCart5,
    FashionMegaMenu: FashionMegaMenu,
    FashionBestDeal1: FashionBestDeal1,
    FashionBestDeal2: FashionBestDeal2,
    FashionBestDeal3: FashionBestDeal3,
    FashionBestDeal4: FashionBestDeal4,
    FashionBestDeal5: FashionBestDeal5,
    FashionBestDeal6: FashionBestDeal6,
    FashionSliderImage1: FashionSliderImage1,
    FashionSliderImage2: FashionSliderImage2,
    FashionSliderImage3: FashionSliderImage3,
    FashionProduct1: FashionProduct1,
    FashionProduct2: FashionProduct2,
    FashionProduct3: FashionProduct3,
    FashionDealDay1: FashionDealDay1,
    FashionDealDay2: FashionDealDay2,
    FashionDealWeek1: FashionDealWeek1,
    FashionDealWeek2: FashionDealWeek2,
    FashionInsta: FashionInsta,
    FashionFacebook: FashionFacebook,
    FooterLogo: FooterLogo,
    FooterPayment: FooterPayment,

    Home2PopUp: Home2PopUp,
    Home2MegaMenu1: Home2MegaMenu1,
    Home2MegaMenu3: Home2MegaMenu3,
    Home2Slider1: Home2Slider1,
    Home2Slider2: Home2Slider2,
    Home2Slider3: Home2Slider3,
    Home2Product1: Home2Product1,
    Home2Product2: Home2Product2,
    Home2Product3: Home2Product3,
    Home2Trending1: Home2Trending1,
    Home2Trending2: Home2Trending2,
    Home2Trending3: Home2Trending3,
    Home2Trending4: Home2Trending4,
    Home2Deal1: Home2Deal1,
    Home2Deal2: Home2Deal2,
    Home2Deal3: Home2Deal3,
    Home2Footer1: Home2Footer1,
    Home2Footer2: Home2Footer2,
    Home2Footer3: Home2Footer3,
    Home2Footer4: Home2Footer4,
    Home3Popup: Home3Popup,
    FashionMegaMenu2: FashionMegaMenu2,
    Home3Slider1: Home3Slider1,
    Home3Slider2: Home3Slider2,
    Home3Slider3: Home3Slider3,
    Home3Category1: Home3Category1,
    Home3Category2: Home3Category2,
    Home3Category3: Home3Category3,
    Home3Category4: Home3Category4,
    Home3Category5: Home3Category5,
    Home3Category6: Home3Category6,
    Home3Category7: Home3Category7,
    Home3Category8: Home3Category8,
    Home3Category9: Home3Category9,
    Home3Category10: Home3Category10,
    Home3Product19: Home3Product19,
    Home3Product1919: Home3Product1919,
    Home3Product20: Home3Product20,
    Home3Product2020: Home3Product2020,
    Home3Product21: Home3Product21,
    Home3Product2121: Home3Product2121,
    Home3Product18: Home3Product18,
    Home3Product1818: Home3Product1818,
    Home3Comment1: Home3Comment1,
    Home4Slider1: Home4Slider1,
    Home4Slider2: Home4Slider2,
    Home4Slider3: Home4Slider3,
    FashionProductImg1: FashionProductImg1,
    FashionProductImg12: FashionProductImg12,
    FashionProductImg2: FashionProductImg2,
    FashionProductImg22: FashionProductImg22,
    FashionProductImg3: FashionProductImg3,
    FashionProductImg33: FashionProductImg33,
    FashionProductSideImg1: FashionProductSideImg1,
    FashionProductSideImg2: FashionProductSideImg2,
    FashionProductImg4: FashionProductImg4,
    FashionProductImg44: FashionProductImg44,
    FashionProductImg5: FashionProductImg5,
    FashionProductImg55: FashionProductImg55,
    FashionProductImg6: FashionProductImg6,
    FashionProductImg66: FashionProductImg66,
    FashionProductImg7: FashionProductImg7,
    FashionProductImg8: FashionProductImg8,
    FashionProductImg9: FashionProductImg9,
    FashionProductImg10: FashionProductImg10,
    FashionProductImg11: FashionProductImg11,
    FashionProductImg12A: FashionProductImg12A,
    FashionProductImg13: FashionProductImg13,
    FashionProductImg14: FashionProductImg14,
    FashionProductImg15: FashionProductImg15,
    FashionProductImg16: FashionProductImg16,
    FashionProductImg1616: FashionProductImg1616,
    FashionProductImg17: FashionProductImg17,
    FashionProductImg1717: FashionProductImg1717,
    FashionProductImg24: FashionProductImg24,
    FashionProductImg2424: FashionProductImg2424,
    FashionPurse3: FashionPurse3,
    FashionShoes1: FashionShoes1,
    FashionShoes2: FashionShoes2,
    FashionBlog1Img: FashionBlog1Img,
    FashionUserImg: FashionUserImg,
    FashionPrev: FashionPrev,
    FashionNext: FashionNext,
    FashionCl: FashionCl,
    FashionC2: FashionC2,
    FashionC3: FashionC3,
    FashionC4: FashionC4,
    FashionC5: FashionC5,
    FashionRecomBlog1: FashionRecomBlog1,
    FashionRecomBlog2: FashionRecomBlog2,
    FashionBlog1: FashionBlog1,
    FashionBlog2: FashionBlog2,
    FashionBlog3: FashionBlog3,
    FashionBlog4: FashionBlog4,
    FashionBlog5: FashionBlog5,
    FashionBlog6: FashionBlog6,
    CategoryGrid: CategoryGrid,
    CategoryGridWhite: CategoryGridWhite,
    CategoryFashionList: CategoryFashionList,
    CategoryFashionListWhite: CategoryFashionListWhite,
    CategoryProduct: CategoryProduct,
    CategoryTShirt1: CategoryTShirt1,
    CategoryTShirt2: CategoryTShirt2,
    CategoryTShirt3: CategoryTShirt3,
    CategoryTShirt4: CategoryTShirt4,
    CategoryTShirt5: CategoryTShirt5,
    CategoryTShirt6: CategoryTShirt6,
    CategoryTShirt7: CategoryTShirt7,
    CategoryTShirt8: CategoryTShirt8,
    CategoryShirt1: CategoryShirt1,
    CategoryShirt2: CategoryShirt2,
    CategoryShirt3: CategoryShirt3,
    CategoryShirt4: CategoryShirt4,
    CategoryShirt5: CategoryShirt5,
    CategoryShirt6: CategoryShirt6,
    CategoryShirt7: CategoryShirt7,
    CategoryShirt8: CategoryShirt8,
    CategoryJeans1: CategoryJeans1,
    CategoryJeans2: CategoryJeans2,
    CategoryJeans3: CategoryJeans3,
    CategoryJeans4: CategoryJeans4,
    CategoryJeans5: CategoryJeans5,
    CategoryJeans6: CategoryJeans6,
    CategoryJeans7: CategoryJeans7,
    CategoryJeans8: CategoryJeans8,
    CategoryJacket1: CategoryJacket1,
    CategoryJacket2: CategoryJacket2,
    CategoryJacket3: CategoryJacket3,
    CategoryJacket4: CategoryJacket4,
    CategoryJacket5: CategoryJacket5,
    CategoryJacket6: CategoryJacket6,
    CategoryJacket7: CategoryJacket7,
    CategoryJacket8: CategoryJacket8,
    CategoryShoes2: CategoryShoes2,
    CategoryShoes3: CategoryShoes3,
    CategoryShoes4: CategoryShoes4,
    CategoryShoes5: CategoryShoes5,
    CategoryShoes6: CategoryShoes6,
    CategoryShoes7: CategoryShoes7,
    CategoryShoes8: CategoryShoes8,
    CategoryGlasses1: CategoryGlasses1,
    CategoryGlasses2: CategoryGlasses2,
    CategoryGlasses3: CategoryGlasses3,
    CategoryGlasses4: CategoryGlasses4,
    CategoryGlasses5: CategoryGlasses5,
    CategoryGlasses6: CategoryGlasses6,
    CategoryGlasses7: CategoryGlasses7,
    CategoryGlasses8: CategoryGlasses8,
    CategoryBag1: CategoryBag1,
    CategoryBag2: CategoryBag2,
    CategoryBag3: CategoryBag3,
    CategoryBag4: CategoryBag4,
    CategoryWomenTShirt1: CategoryWomenTShirt1,
    CategoryWomenTShirt2: CategoryWomenTShirt2,
    CategoryWomenTShirt3: CategoryWomenTShirt3,
    CategoryWomenTShirt4: CategoryWomenTShirt4,
    CategoryWomenTShirt5: CategoryWomenTShirt5,
    CategoryWomenTShirt6: CategoryWomenTShirt6,
    CategoryWomenTShirt7: CategoryWomenTShirt7,
    CategoryWomenTShirt8: CategoryWomenTShirt8,
    CategoryWomenShirt1: CategoryWomenShirt1,
    CategoryWomenShirt2: CategoryWomenShirt2,
    CategoryWomenShirt3: CategoryWomenShirt3,
    CategoryWomenShirt4: CategoryWomenShirt4,
    CategoryWomenShirt5: CategoryWomenShirt5,
    CategoryWomenShirt6: CategoryWomenShirt6,
    CategoryWomenShirt7: CategoryWomenShirt7,
    CategoryWomenShirt8: CategoryWomenShirt8,
    CategoryWomenJeans1: CategoryWomenJeans1,
    CategoryWomenJeans2: CategoryWomenJeans2,
    CategoryWomenJeans3: CategoryWomenJeans3,
    CategoryWomenJeans4: CategoryWomenJeans4,
    CategoryWomenJeans5: CategoryWomenJeans5,
    CategoryWomenJeans6: CategoryWomenJeans6,
    CategoryWomenJeans7: CategoryWomenJeans7,
    CategoryWomenJeans8: CategoryWomenJeans8,
    CategoryWomenShoes2: CategoryWomenShoes2,
    CategoryWomenShoes4: CategoryWomenShoes4,
    CategoryWomenShoes5: CategoryWomenShoes5,
    CategoryWomenShoes6: CategoryWomenShoes6,
    CategoryWomenLeggings1: CategoryWomenLeggings1,
    CategoryWomenLeggings2: CategoryWomenLeggings2,
    CategoryWomenLeggings3: CategoryWomenLeggings3,
    CategoryWomenLeggings4: CategoryWomenLeggings4,
    CategoryWomenLeggings5: CategoryWomenLeggings5,
    CategoryWomenLeggings6: CategoryWomenLeggings6,
    CategoryWomenLeggings7: CategoryWomenLeggings7,
    CategoryWomenLeggings8: CategoryWomenLeggings8,
    CategoryWomenPurse1: CategoryWomenPurse1,
    CategoryWomenPurse2: CategoryWomenPurse2,
    CategoryWomenPurse3: CategoryWomenPurse3,
    CategoryWomenPurse4: CategoryWomenPurse4,
    CategoryWomenPurse5: CategoryWomenPurse5,
    CategoryWomenPurse8: CategoryWomenPurse8,
    FashionBlogGrid24: FashionBlogGrid24,
    FashionBlogGrid23: FashionBlogGrid23,
    FashionBlogGrid22: FashionBlogGrid22,
    FashionBlogGrid21: FashionBlogGrid21,
    FashionBlogGrid20: FashionBlogGrid20,
    FashionBlogGrid19: FashionBlogGrid19,
    FashionBlogGrid18: FashionBlogGrid18,
    FashionBlogGrid17: FashionBlogGrid17,
    FashionBlogGrid16: FashionBlogGrid16,
    FashionBlogGrid15: FashionBlogGrid15,
    FashionBlogGrid14: FashionBlogGrid14,
    FashionBlogGrid13: FashionBlogGrid13,
    FashionBlogGrid12: FashionBlogGrid12,
    FashionBlogGrid11: FashionBlogGrid11,
    FashionBlogGrid10: FashionBlogGrid10,
    FashionBlogGrid9: FashionBlogGrid9,
    FashionBlogGrid8: FashionBlogGrid8,
    FashionBlogGrid7: FashionBlogGrid7,
    FashionBlogGrid6: FashionBlogGrid6,
    FashionBlogGrid5: FashionBlogGrid5,
    FashionBlogGrid4: FashionBlogGrid4,
    FashionBlogGrid3: FashionBlogGrid3,
    FashionBlogGrid2: FashionBlogGrid2,
    FashionBlogGrid1: FashionBlogGrid1,
    LibazzList2: LibazzList2,
    LibazzList2White: LibazzList2White,
    Fashion3Grid: Fashion3Grid,
    Fashion3GridWhite: Fashion3GridWhite,
    Fashion2Grid: Fashion2Grid,
    Fashion2White: Fashion2White,
    WishList1: WishList1,
    WishList2: WishList2,
    About: About,
    Magento: Magento,
    OpenCart: OpenCart,
    Wordpress: Wordpress,
    WooCommerce: WooCommerce,
    PrestaShop: PrestaShop,
    Shopify: Shopify,
    FashionParallax: FashionParallax,
    Tech1: Tech1,
    Tech2: Tech2,
    Tech3: Tech3,
    Tech9: Tech9,
    Tech5: Tech5,
    Tech6: Tech6,
    Tech7: Tech7,
    Tech8: Tech8,
    ComingSoon1: ComingSoon1,
    ComingSoon2: ComingSoon2,
    ComingSoon3: ComingSoon3,
    ComingSoon4: ComingSoon4,
    Faq: Faq,
    SignUp: SignUp,
    SignIn: SignIn,
    FashionBlogList1: FashionBlogList1,
    FashionBlogList2: FashionBlogList2,
    FashionBlogList3: FashionBlogList3,
    FashionBlogList4: FashionBlogList4,
    FashionBlogList5: FashionBlogList5,
    FashionBlogList6: FashionBlogList6,
    FashionBlogList7: FashionBlogList7,
    FashionBlogList8: FashionBlogList8,
    Store1: Store1,
    Store2: Store2,
    Store3: Store3,
    Store4: Store4,
    Store5: Store5,
    FashionBlogClassic1: FashionBlogClassic1,
    FashionBlogClassic2: FashionBlogClassic2,
    FashionBlogClassic3: FashionBlogClassic3,
    FashionBlogClassic4: FashionBlogClassic4,
    FashionBlogClassic5: FashionBlogClassic5,
    SupportTeam: SupportTeam,
    BillingTeam: BillingTeam,
    EvaluationsTeam: EvaluationsTeam,
    ContactUs: ContactUs,
    Fashion6Grid: Fashion6Grid,
    Fashion6GridWhite: Fashion6GridWhite,
    FashionRecom1: FashionRecom1,
    FashionRecom2: FashionRecom2,
    FashionProductimg18: FashionProductimg18,
    FashionProductimg1818: FashionProductimg1818,
    FashionProductimg19: FashionProductimg19,
    FashionProductimg1919: FashionProductimg1919,
    FashionProductimg20: FashionProductimg20,
    FashionProductimg2020: FashionProductimg2020,
    FashionProductimg21: FashionProductimg21,
    FashionProductimg2121: FashionProductimg2121,
    FashionProductimg22: FashionProductimg22,
    FashionProductimg2222: FashionProductimg2222,
    FashionProductimg2323: FashionProductimg2323,
    FashionProductimg23: FashionProductimg23,
    AppleIcon: AppleIcon,
    AndroidIcon: AndroidIcon,
    WindowsIcon: WindowsIcon,
    FashionCbanner1: FashionCbanner1,
    FashionCbanner2: FashionCbanner2,
    FashionSlidebarImg1: FashionSlidebarImg1,
    FashionSlidebar10: FashionSlidebar10,
    FashionSlidebar11: FashionSlidebar11,
    FashionSlidebar12: FashionSlidebar12,
    FashionBannerProduct1: FashionBannerProduct1,
    FashionBannerProduct2: FashionBannerProduct2,
    FashionBannerProduct3: FashionBannerProduct3,
    FashionBannerProduct4: FashionBannerProduct4,
    FashionCeo: FashionCeo,
    FashionFounder: FashionFounder,
    FashionDesigner: FashionDesigner,
    SlidebarImg2: SlidebarImg2,
    SlidebarImg3: SlidebarImg3,
    BannerProduct5: BannerProduct5,
    BannerProduct6: BannerProduct6,
    MobileHomePage1: MobileHomePage1,
    MobileHomePage2: MobileHomePage2,
    MobileHomePage3: MobileHomePage3,
    MobileHomePage4: MobileHomePage4,
    MobileHomePage5: MobileHomePage5,
    MobileHomePage6: MobileHomePage6,
    MobileHomePage7: MobileHomePage7,
    LandMaster: LandMaster,
    LandMacrohtml: LandMacrohtml,
    LandMailo: LandMailo,
    LandMacroshopify: LandMacroshopify,
    EmptySearch: EmptySearch
  }
};
